
import { render } from "react-dom";
import React from "react";
import ProductItem from "../components/Product";

document.addEventListener("DOMContentLoaded", () => {
  if (document.querySelector(".category-navigation")) {
    const node = document.querySelector(".product-item");

    render(<ProductItem />, node);
  }
});
