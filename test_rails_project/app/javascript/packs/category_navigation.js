
import { render } from "react-dom";
import React from "react";
import Header from "../components/CN";

document.addEventListener("DOMContentLoaded", () => {
  if (document.querySelector(".category-navigation")) {
    const node = document.querySelector(".category-navigation");

    render(<Header />, node);
  }
});
