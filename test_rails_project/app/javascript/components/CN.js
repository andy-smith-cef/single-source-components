import React, {Suspense, lazy, useState, useEffect} from 'react';

const categories = [
  {
  name: 'Cable & Accessories',
  slug: '/catalogue/categories/cables-and-accessories',
  children: [{name: 'test child one', slug: '/test-child-one'}, {name: 'test child two', slug: '/test-child-two'}]
  },
  {
  name: 'Cable Management',
  slug: '/catalogue/categories/cable-management',
  children: [{name: 'test child one', slug: '/test-child-one'}, {name: 'test child two', slug: '/test-child-two'}]
  },
  {
  name: 'CCTV, Fire & Security',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Data & Networking',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Domestic & Smart Home',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Heating & Ventilation',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Industrial Controls',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Lamps & Tubes',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Lighting Luminaires',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Switchgear & Distribution',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Test Equipment',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Tools & Fixings',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Wiring Accessories',
  slug: '/test-3',
  children: []
  },
  {
  name: 'Workwear, PPE & Safety',
  slug: '/test-3',
  children: []
  }
  ];

const loadComponent = (scope, module) => {
  return async () => {
    // Initializes the share scope. This fills it with known provided modules from this build and all remotes
    await __webpack_init_sharing__("default");

    const container = window[scope]; // or get the container somewhere else
    // Initialize the container, it may provide shared modules
    await container.init(__webpack_share_scopes__.default);
    const factory = await window[scope].get(module);
    const Module = factory();
    return Module;
  };
}

const useDynamicScript = (args) => {
  const [ready, setReady] = useState(false);
  const [failed, setFailed] = useState(false);

  useEffect(() => {
    if (!args.url) {
      return;
    }

    if (!document.getElementById('remote-source')) {
      console.log('beeeee')
      const element = document.createElement("script");

      element.src = args.url;
      element.id = 'remote-source';
      element.type = "text/javascript";
      element.async = true;

      setReady(false);
      setFailed(false);

      element.onload = () => {
        console.log(`Dynamic Script Loaded: ${args.url}`);
        setReady(true);
      };

      element.onerror = () => {
        console.error(`Dynamic Script Error: ${args.url}`);
        setReady(false);
        setFailed(true);
      };

      document.head.appendChild(element);
    }else {
      setReady(true);
      setFailed(false);
    }

    return () => {
      console.log(`Dynamic Script Removed: ${args.url}`);
      document.head.removeChild(element);
    };
  }, [args.url]);

  return {
    ready,
    failed,
  };
};

const  CategoryNavigation = (props) => {
  // const { system } = props;
  // const { url, scope, module } = system;

  const { ready, failed } = useDynamicScript({
    url: props.system && props.system.url,
  });

  if (!props.system) {
    return <h2>Not system specified</h2>;
  }

  if (!ready) {
    return <h2>Loading dynamic script: {props.system.url}</h2>;
  }

  if (failed) {
    return <h2>Failed to load dynamic script: {props.system.url}</h2>;
  }

  const Component = lazy(
    loadComponent(props.system.scope, props.system.module)
  );

  return (
    <React.Suspense fallback="Loading System">
      <Component backgroundColor='#dfe4e8'
        borderBottomColor='#d02239'
        categories={ categories }/>
    </React.Suspense>
  );
}

const Header = () => {
  const [system, setSystem] = useState(undefined);
  useEffect(() => {
    setSystem({
      url: "http://localhost:3001/remoteEntry.js",
      scope: "sourceApp",
      module: "./nav",
    });
  }, []);


  return (
    <div>
      <CategoryNavigation system={system} />
      {/* <Suspense fallback={<div>Loading...</div>}>
        <CategoryNavigation backgroundColor='#dfe4e8'
        borderBottomColor='#d02239'
        categories={ categories }/>
      </Suspense> */}

    </div>
)};

export default Header
