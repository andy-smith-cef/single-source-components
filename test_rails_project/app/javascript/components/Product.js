import React, {Suspense, lazy, useState, useEffect} from 'react';

const loadComponent = (scope, module) => {
  return async () => {
    // Initializes the share scope. This fills it with known provided modules from this build and all remotes
    await __webpack_init_sharing__("default");

    const container = window[scope]; // or get the container somewhere else
    // Initialize the container, it may provide shared modules
    await container.init(__webpack_share_scopes__.default);
    const factory = await window[scope].get(module);
    const Module = factory();
    return Module;
  };
}

const useDynamicScript = (args) => {
  const [ready, setReady] = useState(false);
  const [failed, setFailed] = useState(false);

  useEffect(() => {
    if (!args.url) {
      return;
    }

    if (!document.getElementById('remote-source')) {
      console.log('booooooo')
      const element = document.createElement("script");

      element.src = args.url;
      element.id = 'remote-source';
      element.type = "text/javascript";
      element.async = true;

      setReady(false);
      setFailed(false);

      element.onload = () => {
        console.log(`Dynamic Script Loaded: ${args.url}`);
        setReady(true);
      };

      element.onerror = () => {
        console.error(`Dynamic Script Error: ${args.url}`);
        setReady(false);
        setFailed(true);
      };

      document.head.appendChild(element);
    }else {
      console.log('i am here now')
      setReady(true);
      setFailed(false);
    }

    console.log('i am ------', ready)

    return () => {
      console.log(`Dynamic Script Removed: ${args.url}`);
      document.head.removeChild(element);
    };
  }, [args.url]);

  return {
    ready,
    failed,
  };
};

const  Product = (props) => {
  // const { system } = props;
  // const { url, scope, module } = system;
  console.log("here ------",  ready)
  const { ready, failed } = useDynamicScript({
    url: props.system && props.system.url,
  });

  if (!props.system) {
    return <h2>Not system specified</h2>;
  }

  if (!ready) {
    return <h2>Loading dynamic script: {props.system.url}</h2>;
  }

  if (failed) {
    return <h2>Failed to load dynamic script: {props.system.url}</h2>;
  }

  const Component = lazy(
    loadComponent(props.system.scope, props.system.module)
  );

  return (
    <React.Suspense fallback="Loading System">
      <Component/>
    </React.Suspense>
  );
}

const ProductItem = () => {
  const [system, setSystem] = useState(undefined);
  useEffect(() => {
    setSystem({
      url: "http://localhost:3001/remoteEntry.js",
      scope: "sourceApp",
      module: "./product",
    });
  }, []);


  return (
    <div>
      <Product system={system} />
      {/* <Suspense fallback={<div>Loading...</div>}>
        <CategoryNavigation backgroundColor='#dfe4e8'
        borderBottomColor='#d02239'
        categories={ categories }/>
      </Suspense> */}

    </div>
)};

export default ProductItem
