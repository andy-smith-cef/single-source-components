const { webpackConfig, merge } = require("@rails/webpacker");
const { ModuleFederationPlugin } = require("webpack").container;
const deps = require('../../package.json').dependencies;
const ForkTSCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

module.exports = merge(webpackConfig, {
  output: {
    // Makes exports from entry packs available to global scope, e.g.
    library: ['Packs', '[name]'],
    libraryTarget: 'window'
  },
},{
  plugins: [new ForkTSCheckerWebpackPlugin(), new ModuleFederationPlugin({
      name: 'TestApp',
      remoteType: 'var',
      remotes: {
        sourceApp: `sourceApp@//localhost:3001/remoteEntry.js`
      },
      exposes: {},
      shared: {
        ...deps,
        react: {
          singleton: true,
          eager:true,
          requiredVersion: deps.react,
        },
        'react-dom': {
          singleton: true,
          eager:true,
          requiredVersion: deps['react-dom'],
        },
        "@rails/activestorage" : {
          singleton: true,
          eager:true,
          requiredVersion: deps['@rails/activestorage'],
        }
      },
    })],
});
