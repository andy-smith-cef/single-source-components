const { ModuleFederationPlugin } = require("webpack").container;
const deps = require('../../package.json').dependencies;

module.exports = {
  plugins: [
    new ModuleFederationPlugin({
      name: 'TestApp',
      remotes: {
        sourceApp: `sourceApp@//localhost:3001/remoteEntry.js`
      },
      exposes: {},
      shared: {
        ...deps,
        react: {
          singleton: true,
          eager: true,
          requiredVersion: deps.react,
        },
        'react-dom': {
          singleton: true,
          eager: true,
          requiredVersion: deps['react-dom'],
        },
      },
    }),
  ],
};
