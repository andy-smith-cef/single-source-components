import React from 'react';
import {
  CategoryNavigationWrapper,
  CategoryNavigationListItem,
  CategoryNavigationList,
  CategoryNavigationParentLink,
} from './CategoryNavigation.styles';

const CategoryNavigation = ({
  backgroundColor,
  borderBottomColor,
  categories,
}) => (
  <>
    <CategoryNavigationWrapper>
      {categories && (
        <div className="v2-container">
          <CategoryNavigationList data-testid="categoryNavigation">
            {categories.map(category => (
              <CategoryNavigationListItem
                key={category.name.replace(/\s+/g, '-').toLowerCase()}>
                <CategoryNavigationParentLink
                  backgroundColor={backgroundColor}
                  borderBottomColor={borderBottomColor}
                  href={category.slug}>
                  <span>{category.name}</span>
                </CategoryNavigationParentLink>
              </CategoryNavigationListItem>
            ))}
          </CategoryNavigationList>
        </div>
      )}
    </CategoryNavigationWrapper>
  </>
);

export default CategoryNavigation;
