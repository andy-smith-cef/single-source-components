import React from 'react';
import { render } from '@testing-library/react';
import CategoryNavigation from './CategoryNavigation';

const categories = [
  {
    name: 'Test 1',
    slug: '/test-1',
    children: [
      { name: 'test child one', slug: '/test-child-one' },
      { name: 'test child two', slug: '/test-child-two' },
    ],
  },
  {
    name: 'Test 2',
    slug: '/test-2',
    children: [
      { name: 'test child one', slug: '/test-child-one' },
      { name: 'test child two', slug: '/test-child-two' },
    ],
  },
  {
    name: 'Test 3',
    slug: '/test-3',
    children: [],
  },
];

it('CategoryNavigation renders children when they exist', () => {
  const { container } = render(
    <CategoryNavigation
      backgroundColor="#dfe4e8"
      borderBottomColor="#d02239"
      categories={categories}
    />,
  );
  const list = container.querySelector('ul');
  const items = list.querySelectorAll('li');
  expect(items.length).toBe(categories.length);
});
