import React from "react";
  import { ComponentStory, ComponentMeta } from "@storybook/react";
  const Readme = require("../README.md").default;

  import CategoryNavigation from "../CategoryNavigation";


  export default {
    title: "UI Components/Category Navigation",
    component: CategoryNavigation,
    argTypes: {
      backgroundColor: {control: "color"},
      borderBottomColor: {control: "color"},
    },
  } as ComponentMeta<typeof CategoryNavigation>;

  // ==============================
  // Traditional Node Render on Client Side
  // ==============================
const categories = [
        {
        name: 'Cable & Accessories',
        slug: '/catalogue/categories/cables-and-accessories',
        children: [{name: 'test child one', slug: '/test-child-one'}, {name: 'test child two', slug: '/test-child-two'}]
        },
        {
        name: 'Cable Management',
        slug: '/catalogue/categories/cable-management',
        children: [{name: 'test child one', slug: '/test-child-one'}, {name: 'test child two', slug: '/test-child-two'}]
        },
        {
        name: 'CCTV, Fire & Security',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Data & Networking',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Domestic & Smart Home',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Heating & Ventilation',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Industrial Controls',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Lamps & Tubes',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Lighting Luminaires',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Switchgear & Distribution',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Test Equipment',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Tools & Fixings',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Wiring Accessories',
        slug: '/test-3',
        children: []
        },
        {
        name: 'Workwear, PPE & Safety',
        slug: '/test-3',
        children: []
        }
    ]
  const Template: ComponentStory<typeof CategoryNavigation> = (args) => (
    <CategoryNavigation {...args} />
  );

  export const Primary = Template.bind({});
  Primary.args = {
    backgroundColor:'#dfe4e8',
    borderBottomColor:'#d02239',
    categories: categories,
  };
  Primary.parameters = {
    readme: {
      sidebar: Readme,
    },
  };
