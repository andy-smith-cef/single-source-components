import styled from 'styled-components';

const CategoryNavigationWrapper = styled.div`
  padding: 0;
  font-size: 14px;
  margin: 0;
  font-family: sans-serif;
  background-color: #edf1f3;
  display: flex;
  justify-content: center;
  align-items: center;
  > .v2-container {
    max-width: 1190px;
  }
`;

const CategoryNavigationList = styled.ul`
  padding: 0;
  margin: 0;
  list-style: none;
  flex-wrap: nowrap;
  justify-content: center;
  display: flex;
`;

const CategoryNavigationListItem = styled.li`
  position: static;
  border-left: 1px solid white;
  border-right: 1px solid white;
  display: flex;
  flex-grow: 1;
  justify-content: center;
  align-items: center;
  flex: 1 1 7%;
  :hover > a {
    color: #000;
  }
`;

const CategoryNavigationParentLink = styled.a`
  flex: 1;
  align-items: center;
  justify-content: center;
  display: flex;
  text-align: center;
  line-height: 1;
  font-size: 0.8125rem;
  height: 56px;
  color: #535353;
  background-color: ${props =>
    props.backgroundColor !== undefined ? props.backgroundColor : 'white'};
  border-bottom-width: 2px;
  border-bottom-style: solid;
  border-bottom-color: ${props =>
    props.borderBottomColor !== undefined ? props.borderBottomColor : 'red'};
  text-decoration: none;
`;

export {
  CategoryNavigationWrapper,
  CategoryNavigationList,
  CategoryNavigationListItem,
  CategoryNavigationParentLink,
};
