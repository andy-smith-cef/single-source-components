# 📝 Summary

This is an example component intended to outline the expected code quality for a new component introduced to the code base.

# 💻 Usage

```jsx
// Replace the file path with the correct filepath to CategoryNavigation
import CategoryNavigation from './CategoryNavigation/CategoryNavigation';

// Replace the string provided to text with your own
const categories = [
    {
    name: 'Test 1',
    slug: '/test-1',
    children: [{name: 'test child one', slug: '/test-child-one'}, {name: 'test child two', slug: '/test-child-two'}]
    },
    {
    name: 'Test 2',
    slug: '/test-2',
    children: [{name: 'test child one', slug: '/test-child-one'}, {name: 'test child two', slug: '/test-child-two'}]
    },
    {
    name: 'Test 3',
    slug: '/test-3',
    children: []
    }
];


<CategoryNavigation backgroundColor='#dfe4e8' borderBottomColor='#d02239' categories={categories} />
```

# 📩 Category Navigation Props

| Name | Required | Type   | DefaultValue | Description  |
| ---- | -------- | ------ | ------------ | ------------ |
| backgroundColor | ❌       | string | -            | item background color |
| borderBottomColor | ❌       | string | -            | item border color |
| categories | ✅       | array of objects | -            | root categories and children array |
