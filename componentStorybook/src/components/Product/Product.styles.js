import styled from 'styled-components';

const ProductWrapper = styled.div`

  padding: 0;
  font-size: 14px;
  margin: 3rem 0 0 0;
  font-family: sans-serif;
  background-color: #edf1f3;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ProductItem = styled.div`
  background: white;
  width: 300px;
  display: inline-block;
  margin: auto;
  border-radius: 19px;
  position: relative;
  text-align: center;
  box-shadow: -1px 15px 30px -12px black;
  z-index: 9999;

  .clash-card__image {
    position: relative;
    height: 230px;
    margin-bottom: 35px;
    border-top-left-radius: 14px;
    border-top-right-radius: 14px;
  }

  .clash-card__image--barbarian {
    background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/barbarian-bg.jpg');
    img {
      width: 400px;
      position: absolute;
      top: -65px;
      left: -70px;
    }
  }

  .clash-card__level {
    text-transform: uppercase;
    font-size: 12px;
    font-weight: 700;
    margin-bottom: 3px;
  }

  .clash-card__level--barbarian {
    color: #EC9B3B;
  }

  .clash-card__unit-name {
    font-size: 26px;
    color: black;
    font-weight: 900;
    margin-bottom: 5px;
  }

  .clash-card__unit-description {
    padding: 20px;
    margin-bottom: 10px;
  }

  .clash-card__unit-stats--barbarian {
    background: #EC9B3B;

    .one-third {
      border-right: 1px solid #BD7C2F;
    }
  }

  .clash-card__unit-stats {
    color: white;
    font-weight: 700;
    border-bottom-left-radius: 14px;
    border-bottom-right-radius: 14px;
    display:flex;
    align-items:center;
    justify-content:center;

    .one-third {
      flex-basis:33%;
      padding: 20px 15px;
    }

    sup {
      position: absolute;
      bottom: 4px;
      font-size: 45%;
      margin-left: 2px;
    }

    .stat {
      position: relative;
      font-size: 24px;
      margin-bottom: 10px;
    }

    .stat-value {
      text-transform: uppercase;
      font-weight: 400;
      font-size: 12px;
    }

    .no-border {
      border-right: none;
    }
  }

`;

export {
  ProductWrapper,
  ProductItem,
};
