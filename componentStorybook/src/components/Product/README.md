# 📝 Summary

This is an example component intended to outline the expected code quality for a new component introduced to the code base.

# 💻 Usage

```jsx
// Replace the file path with the correct filepath to CategoryNavigation
import Product from './Product/Product';

// Replace the string provided to text with your own

<Product />
```
