import React from "react";
import { render } from "@testing-library/react";
import Product from "./Product";

it("Product renders text prop", () => {
    const { getByText } = render(
    <Product text={"Hello World from test"} />
    );
    expect(getByText("Hello World from test")).toBeTruthy();
});

it("Product renders with no prop value provided", () => {
    const { getByText } = render(<Product text={""} />);
    expect(getByText("no prop value provided")).toBeTruthy();
});