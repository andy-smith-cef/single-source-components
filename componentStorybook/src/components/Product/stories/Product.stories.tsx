import React from "react";
  import { ComponentStory, ComponentMeta } from "@storybook/react";
  const Readme = require("../README.md").default;

  import Product from "../Product";

  export default {
    title: "UI Components//Product/Default",
    component: Product,

  } as ComponentMeta<typeof Product>;

  // ==============================
  // Traditional Node Render on Client Side
  // ==============================

  const Template: ComponentStory<typeof Product> = () => (
    <Product/>
  );

  export const Primary = Template.bind({});

  Primary.parameters = {
    readme: {
      sidebar: Readme,
    },
  };
