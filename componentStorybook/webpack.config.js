const { ModuleFederationPlugin } = require('webpack').container;

const deps = require('./package.json').dependencies;

module.exports = {
  resolve: {
    extensions: ['.tsx', '.ts', '.jsx', '.js', '.json', '.md'],
  },

  externalsPresets: {
    web: false,
    webAsync: true,
  },

  output: {
    publicPath: 'http://localhost:3001/',
  },

  devServer: {
    port: 3001,
  },

  module: {
    rules: [
      {
        test: /\.m?js/,
        type: 'javascript/auto',
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },

  plugins: [
    new ModuleFederationPlugin({
      name: 'sourceApp',
      filename: 'remoteEntry.js',
      library: { type: 'var', name: 'sourceApp' },
      remotes: {},
      exposes: {
        './nav': './src/components/CategoryNavigation',
        './product': './src/components/Product',
      },
      shared: {
        ...deps,
        react: {
          singleton: true,
          eager: true,
          requiredVersion: deps.react,
        },
        'react-dom': {
          singleton: true,
          eager: true,
          requiredVersion: deps['react-dom'],
        },
        'styled-components': {
          singleton: true,
          eager:true,
          requiredVersion: deps['styled-components'],
        },
      },
    }),
  ],
};
